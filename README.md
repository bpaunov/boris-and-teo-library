Single page application built to handle the primary library provesses and functions like stocking, tracking and renting of books.
The application is divided into 2 sections:
Common user - able to search for and rent/return books as well as to leave book reviews or to like/dislike other users' reviews.
Admin section - where the admin can do things like add books, delete them, increase the quantity of a book etc., as well as to perform CRUD operations onto the common users.
The system implements scoring and gamification functionality which awards the users with points and levels corresponding to the points accrued.
