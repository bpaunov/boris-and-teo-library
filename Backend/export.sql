-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema blacklist_tokens_library
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema blacklist_tokens_library
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `blacklist_tokens_library` DEFAULT CHARACTER SET latin1 ;
-- -----------------------------------------------------
-- Schema library
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema library
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `library` DEFAULT CHARACTER SET latin1 ;
USE `blacklist_tokens_library` ;

-- -----------------------------------------------------
-- Table `blacklist_tokens_library`.`is_blacklisted`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blacklist_tokens_library`.`is_blacklisted` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `token` LONGTEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;

USE `library` ;

-- -----------------------------------------------------
-- Table `library`.`books`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`books` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `author` VARCHAR(45) NOT NULL,
  `is_deleted` TINYINT(4) NOT NULL,
  `year` INT(11) NOT NULL,
  `genre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `library`.`levels`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`levels` (
  `id` INT(11) NOT NULL,
  `level_name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `library`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`roles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `role_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `library`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`users` (
  `username` VARCHAR(45) NOT NULL,
  `password` LONGTEXT NOT NULL,
  `is_deleted` TINYINT(4) NOT NULL,
  `roles_id` INT(11) NOT NULL,
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `levels_id` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`username`, `roles_id`, `levels_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_users_roles1_idx` (`roles_id` ASC) VISIBLE,
  INDEX `fk_users_levels1_idx` (`levels_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_levels1`
    FOREIGN KEY (`levels_id`)
    REFERENCES `library`.`levels` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_roles1`
    FOREIGN KEY (`roles_id`)
    REFERENCES `library`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `library`.`is_banned`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`is_banned` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `description` LONGTEXT NOT NULL,
  `start_date` DATETIME NOT NULL,
  `expiration_date` DATETIME NOT NULL,
  `is_deleted` TINYINT(4) NOT NULL,
  `duration` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `users_id`),
  INDEX `fk_is_banned_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_is_banned_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `library`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 22
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `library`.`is_borrowed`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`is_borrowed` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `users_username` VARCHAR(45) NOT NULL,
  `books_id` INT(11) NOT NULL,
  `is_deleted` TINYINT(4) NOT NULL,
  PRIMARY KEY (`id`, `users_username`, `books_id`),
  INDEX `fk_is_borrowed_users1_idx` (`users_username` ASC) VISIBLE,
  INDEX `fk_is_borrowed_books1_idx` (`books_id` ASC) VISIBLE,
  CONSTRAINT `fk_is_borrowed_books1`
    FOREIGN KEY (`books_id`)
    REFERENCES `library`.`books` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_is_borrowed_users1`
    FOREIGN KEY (`users_username`)
    REFERENCES `library`.`users` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `library`.`is_not_listed`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`is_not_listed` (
  `id` INT(11) NOT NULL,
  `books_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `books_id`),
  INDEX `fk_is_not_listed_books1_idx` (`books_id` ASC) VISIBLE,
  CONSTRAINT `fk_is_not_listed_books1`
    FOREIGN KEY (`books_id`)
    REFERENCES `library`.`books` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `library`.`points`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`points` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `value` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  `users_username` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`, `users_id`, `users_username`),
  INDEX `fk_points_users1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_points_users2_idx` (`users_username` ASC) VISIBLE,
  CONSTRAINT `fk_points_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `library`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_points_users2`
    FOREIGN KEY (`users_username`)
    REFERENCES `library`.`users` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `library`.`ratings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`ratings` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `rating` INT(11) NOT NULL,
  `books_id` INT(11) NOT NULL,
  `users_username` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`, `books_id`, `users_username`),
  INDEX `fk_ratings_books1_idx` (`books_id` ASC) VISIBLE,
  INDEX `fk_ratings_users1_idx` (`users_username` ASC) VISIBLE,
  CONSTRAINT `fk_ratings_books1`
    FOREIGN KEY (`books_id`)
    REFERENCES `library`.`books` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ratings_users1`
    FOREIGN KEY (`users_username`)
    REFERENCES `library`.`users` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `library`.`reviews`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`reviews` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `content` VARCHAR(300) NOT NULL,
  `books_id` INT(11) NOT NULL,
  `users_username` VARCHAR(45) NOT NULL,
  `created_at` DATE NOT NULL,
  `is_deleted` TINYINT(4) NOT NULL,
  PRIMARY KEY (`id`, `books_id`, `users_username`),
  INDEX `fk_review_book1_idx` (`books_id` ASC) VISIBLE,
  INDEX `fk_review_user1_idx` (`users_username` ASC) VISIBLE,
  CONSTRAINT `fk_review_book1`
    FOREIGN KEY (`books_id`)
    REFERENCES `library`.`books` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_review_user1`
    FOREIGN KEY (`users_username`)
    REFERENCES `library`.`users` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `library`.`sessions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`sessions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `is_deleted` TINYINT(4) NOT NULL,
  `users_username` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`, `users_username`),
  INDEX `fk_sessions_users1_idx` (`users_username` ASC) VISIBLE,
  CONSTRAINT `fk_sessions_users1`
    FOREIGN KEY (`users_username`)
    REFERENCES `library`.`users` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 44
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `library`.`votes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`votes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `status` TINYINT(4) NULL DEFAULT NULL,
  `reviews_id` INT(11) NOT NULL,
  `users_username` VARCHAR(45) NOT NULL,
  `is_deleted` TINYINT(4) NOT NULL,
  PRIMARY KEY (`id`, `reviews_id`, `users_username`),
  INDEX `fk_votes_review1_idx` (`reviews_id` ASC) VISIBLE,
  INDEX `fk_votes_user1_idx` (`users_username` ASC) VISIBLE,
  CONSTRAINT `fk_votes_review1`
    FOREIGN KEY (`reviews_id`)
    REFERENCES `library`.`reviews` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_votes_user1`
    FOREIGN KEY (`users_username`)
    REFERENCES `library`.`users` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 55
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
