INSERT INTO `library.roles` (`role_name`) VALUES (`Admin`);
INSERT INTO `library.roles` (`role_name`) VALUES (`Common`);

INSERT INTO `library.levels` (`level_name`) VALUES (`regular`);
INSERT INTO `library.levels` (`level_name`) VALUES (`bronze`);
INSERT INTO `library.levels` (`level_name`) VALUES (`silver`);
INSERT INTO `library.levels` (`level_name`) VALUES (`gold`);
INSERT INTO `library.levels` (`level_name`) VALUES (`diamond`);

INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`Harry Potter and the Philosopher's Stone`, `J. K. Rowling`, `1997`, `ADVENTURE`, false);
INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`Harry Potter and the Chamber of Secrets`, `J. K. Rowling`, `1998`, `ADVENTURE`, false);
INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`Harry Potter and the Prisoner of Azkaban`, `J. K. Rowling`, `1999`, `ADVENTURE`, false);
INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`Harry Potter and the Goblet of Fire`, `J. K. Rowling`, `2000`, `ADVENTURE`, false);
INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`Harry Potter and the Order of the Phoenix`, `J. K. Rowling`, `2003`, `ADVENTURE`, false);
INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`Harry Potter and the Half-Blood Prince`, `J. K. Rowling`, `2005`, `ADVENTURE`, false);
INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`Harry Potter and the Deathly Hallows`, `J. K. Rowling`, `2007`, `ADVENTURE`, false);
INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`The Color Purple`, `Alice Walker`, `1987`, `DRAMA`, false);

INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`A Game of Thrones`, `George R. R. Martin`, `1996`, `FANTASY`, false);
INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`A Clash of Kings`, `George R. R. Martin`, `1998`, `FANTASY`, false);
INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`A Storm of Swords`, `George R. R. Martin`, `2000`, `FANTASY`, false);
INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`A Feast for Crows`, `George R. R. Martin`, `2005`, `FANTASY`, false);
INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`A Dance with Dragons`, `George R. R. Martin`, `2011`, `FANTASY`, false);
INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`The Winds of Winter`, `George R. R. Martin`, `9999`, `FANTASY`, false);
INSERT INTO `library.is_not_listed` (`books_id`) VALUES (SELECT `id` FROM `books` WHERE `title`= `A Dance with Dragons`);
INSERT INTO `library.books` (`title`, `author`, `year`, `genre`, `is_deleted`) VALUES (`A Dream of Spring`, `George R. R. Martin`, `9999`, `FANTASY`, false);
INSERT INTO `library.is_not_listed` (`books_id`) VALUES (SELECT `id` FROM `books` WHERE `title`= `A Dream of Spring`);


