import { poolBlacklist } from '../config.js'


const check_B_List = async (token) => {
    const sql = await poolBlacklist.query(`select * from is_blacklisted where token = '${token}'`);
    return sql[0];
}

export const checkBlacklist = async (req, res, next) => {
    const check = await check_B_List(req.headers.authorization.split(' ')[1])
    if (check) {
        res.status(401).send({ message: 'Unauthorized' });
    } else {
        next();
    }
}