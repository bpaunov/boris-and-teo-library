import { pool, poolBlacklist } from '../config.js';

const getUser = async (username) => {
    const sql = await pool.query(`
    SELECT * from users 
    WHERE username = '${username}' &&
    is_deleted = 0`);

    return sql[0];
}

const getUserById = async (id) => {
    const sql = await pool.query(`
    SELECT * from users 
    WHERE id = ${id} &&
    is_deleted = 0`);

    return sql[0];
}

const addNewSession = async (username) => {
    const check = await pool.query(`
    SELECT *
    FROM sessions
    WHERE users_username = '${username}'
    && is_deleted = false;
    `)
    if (!check[0]) {
        await pool.query(`
    INSERT INTO sessions (users_username, is_deleted)
    VALUES ('${username}', false);`)

        const newSession = await pool.query(`
    SELECT *
    FROM sessions
    WHERE users_username = '${username}'
    && is_deleted = false;
    `);
        await pool.query(`
    CREATE EVENT IF NOT EXISTS token_expiration_event${newSession[0].id}
    ON SCHEDULE AT CURRENT_TIMESTAMP + INTERVAL 60 MINUTE
    DO
    UPDATE sessions SET is_deleted = true 
    WHERE users_username = '${username}' && is_deleted = false;
    `);
        return newSession[0];
    }
    return null;
}
const createUser = async (userInfo) => {
    await pool.query(`
    INSERT INTO users 
    (username, password, roles_id, is_deleted) 
    VALUES ('${userInfo.username}', '${userInfo.password}', 
    (SELECT id FROM roles WHERE role_name = '${userInfo.role}'), false);`);
    const createdUserInfo = await pool.query(`select username from users where username = '${userInfo.username}'`);
    return {
        username: createdUserInfo[0].username
    }
}

const logOut = async (token, username) => {
    const currentSessionId = await pool.query(`select id from sessions where users_username = '${username}' and is_deleted = 0`);

    await pool.query(`update sessions set is_deleted = true where users_username = '${username}' and is_deleted = 0`);

    const deletedCurrentSession = await pool.query(`select * from sessions where id = ${currentSessionId[0].id}`);

    await poolBlacklist.query(`insert into is_blacklisted (token) values ('${token}')`);

    return deletedCurrentSession[0];
}

const banUser = async (id, reason, duration) => {
    const check = await pool.query(`
    SELECT *
    FROM  is_banned
    WHERE users_id = ${id}
    && is_deleted = false;
    `)
    if (check[0]) {
        return null;
    }
    await pool.query(`
        INSERT INTO is_banned
        ( users_id, description, start_date, expiration_date, is_deleted, duration)
        VALUES (${id}, '${reason}', NOW(), DATE_ADD(now(), INTERVAL ${duration} MINUTE), false, ${duration});`);
    const num = await pool.query(`SELECT id FROM is_banned WHERE users_id = ${id} && is_deleted = false`)
    await pool.query(`CREATE EVENT IF NOT EXISTS ban_event${num[0].id}
        ON SCHEDULE AT CURRENT_TIMESTAMP + INTERVAL ${duration} MINUTE
        DO
        UPDATE is_banned set expiration_date = now(), is_deleted = true 
        WHERE users_id = ${id} && description = '${reason}' && is_deleted = 0;
        `);
    const bannedUser = await pool.query(`select * from users WHERE id = ${id}`);
    return bannedUser[0];
}

const banPointsDeduction = async (id, duration) => {
    const sql = `
    UPDATE points
    SET value = (value*0.75)-(${duration}*5)
    WHERE users_id = ${id}
    `
    await pool.query(sql);
    const check = await pool.query(`
    SELECT value
    FROM points
    WHERE users_id = ${id}
    `)
    if (check[0].value < 0) {
        await pool.query(`
        UPDATE points
        SET value = 0
        WHERE users_id = ${id}
        `)
    }
}

const deleteUser = async (id) => {
    await pool.query(`
    UPDATE users
    SET is_deleted = true
    WHERE id = ${id};
    `)
    const deletedUser = await pool.query(`
    SELECT * 
    FROM users
    WHERE id = ${id};
    `)

    return deletedUser[0];
}

const checkPoints = async (id) => {
    const check = await pool.query(`
    SELECT value
    FROM points
    WHERE users_id = ${id}
    `)
    return check[0];
}

const updateLevel = async (id, level) => {
    const sql = `
    UPDATE users
    SET levels_id = ${level}
    WHERE id = ${id};
    `
    await pool.query(sql);
}

const createRecords = async (username, userId) => {
    await pool.query(`
    INSERT INTO points (value, users_username, users_id)
    VALUES (${0}, '${username}', ${userId});`)

    const sql = (`SELECT * FROM points
    WHERE users_username = '${username}'
    `)
    return sql[0].id;
}

export default {
    getUserById,
    createUser,
    addNewSession,
    getUser,
    logOut,
    banUser,
    deleteUser,
    updateLevel,
    checkPoints,
    banPointsDeduction,
    createRecords
}