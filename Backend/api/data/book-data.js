import { pool } from '../config.js';
import userService from '../services/user-service.js';


const getAllBooks = async () => {
    const result = await pool.query(`
    SELECT b.id, b.title, b.author, b.year, b.genre, b.is_deleted, b.img_src, i.id as borrowed_ID, n.id as unlisted_ID
    FROM books b
    left JOIN is_borrowed i ON b.id = i.books_id && i.is_deleted = false
    left JOIN is_not_listed n ON b.id = n.books_id
    having b.is_deleted = 0;
    `);
    return result;
}

const bookById = async (id) => {
    const sql = await pool.query(`
    SELECT * 
    FROM books 
    WHERE id = ${id}
    && is_deleted = false;`);
    return sql[0];
};

const borrow = async (obj) => {
    console.log(obj)
    const exists = await pool.query(`
    SELECT * 
    FROM is_borrowed
    WHERE books_id = ${obj.id}
    && is_deleted = false;
    `);
    if (exists[0]) {
        return null
    }
    await pool.query(`
    INSERT INTO is_borrowed (books_id, users_username, is_deleted) 
    VALUES (${obj.id}, '${obj.user}', false);
    `)
    const borrowing = await pool.query(`
    SELECT *
    FROM is_borrowed
    WHERE books_id = ${obj.id}
    && is_deleted = false;
    `);
    return borrowing[0];
}

const returnBook = async (obj) => {
    const check = await pool.query(`
    SELECT id 
    FROM is_borrowed 
    WHERE books_id = ${obj.id}
    && users_username = '${obj.user}'`);
    if (!check[0]) {
        return null;
    } else {
        await pool.query(`
        UPDATE is_borrowed 
        SET is_deleted = true
        WHERE books_id = ${obj.id} 
        && users_username = '${obj.user}'
        && is_deleted = false
        ;`)
        const check = await pool.query(`SELECT *
        FROM points
        WHERE users_username = '${obj.user}'
        `)
        if (check[0] === undefined) {
            await pool.query(`
        INSERT INTO points
        (users_username, value)
        VALUES
        ('${obj.user}', 7)
        `)
        } else {
            await pool.query(`
        UPDATE points
        SET value = value + 7
        WHERE users_username = '${obj.user}'
        `
            )
        }
    }
    const borrowing = await pool.query(`
    SELECT * 
    FROM is_borrowed 
    WHERE books_id = ${obj.id}
    && users_username = '${obj.user}'
    && is_deleted = true`);
    return borrowing.pop();
}


const addBookReview = async (reviewObj) => {

    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDay();
    ///////////////////////////////////////////////////
    await pool.query(`
    INSERT INTO reviews
    (content, books_id, users_username, created_at, is_deleted)
    VALUES ('${reviewObj.content}', ${reviewObj.bookId}, '${reviewObj.username}', '${year}-${month}-${day}', false);
    `);
    const addedReview =
        await pool.query(`
            SELECT * from reviews r
            WHERE books_id = ${reviewObj.bookId}
            && users_username = '${reviewObj.username}'
            && content = '${reviewObj.content}'
            && created_at = '${year}-${month}-${day}';
            `);
    ///////////////////////////////////////////////////
    const check = await pool.query(`
    SELECT *
    FROM points
    WHERE users_username = '${reviewObj.username}'
    `)
    if (check === undefined) {
        await pool.query(`
        INSERT INTO points
        (users_username, value)
        VALUES
        ('${reviewObj.username}', 15)
        `)
    } await pool.query(`
    UPDATE points
    SET value = value + 15
    WHERE users_username = '${reviewObj.username}'
    `)
    //await userData.checkForLevel(reviewObj.username);
    return addedReview[0];
}
const reviewById = async (reviewId) => {
    const review = await pool.query(`
    SELECT *
    FROM reviews 
    WHERE id = '${reviewId}'
    && is_deleted = 0;
    `);
    return review[0];
}

const allReviews = async (bookId) => {
    const reviews = await pool.query(`
    SELECT *
    FROM reviews 
    WHERE books_id = ${bookId}
    && is_deleted = 0;
    `);
    return reviews
}

const updateReview = async (reviewId, content) => {
    const sql = `
    SELECT *
    FROM reviews 
    WHERE id = ${reviewId}
    `;
    const review = await pool.query(sql);
    if (!review) {
        return null;
    }
    const sql2 = `
    UPDATE reviews
    SET content = '${content}'
    WHERE id = ${reviewId}
    `;
    await pool.query(sql2)
    const updatedReview = await pool.query(sql);
    return updatedReview[0];
}

const deleteReview = async (obj) => {
    ///////////////////////////////////////////////////
    const sql = await pool.query(`
    SELECT * from reviews 
    WHERE id = ${obj.reviewId} 
    && users_username = '${obj.username}' 
    && books_id = ${obj.bookId}
    `);
    if (!sql[0]) {
        return undefined;
    } else if (sql[0].is_deleted === 1) {
        return null;
    } else {
        await pool.query(`
        UPDATE reviews 
        SET is_deleted = true 
        WHERE id = ${obj.reviewId}
        `);
        const returnDeleted = await pool.query(`
        SELECT * from reviews 
        WHERE id = ${obj.reviewId}
        `);
        return returnDeleted[0];
    }
    ///////////////////////////////////////////////////
}
const likeReview = async (obj) => {

    const check = await pool.query(`
    SELECT * 
    FROM votes
    WHERE users_username = '${obj.username}'
    && reviews_id = ${obj.reviewId}
    `);

    if (check.length === 0) {
        const sql = `
        INSERT INTO votes
        (status, reviews_id, users_username, is_deleted)
        VALUES
        (${obj.like}, ${obj.reviewId}, '${obj.username}', 0)
        `
        await pool.query(sql);

        if (obj.like === 1) {
            const sql3 = `
                UPDATE points
                SET value = value + 3
                WHERE users_username =(SELECT users_username 
                FROM reviews
                WHERE id = ${obj.reviewId} )
                `
            await pool.query(sql3);
        } else {
            const sql3 = `
                UPDATE points
                SET value = value - 3
                WHERE users_username =(SELECT users_username 
                FROM reviews
                WHERE id = ${obj.reviewById} )
                `
            await pool.query(sql3);
        }

    } else {
        const check = await pool.query(`
        SELECT status 
        FROM votes 
        WHERE users_username = '${obj.username}' 
        && reviews_id = ${obj.reviewId}`)
        if (check[0].status === obj.like) {
            return null;
        }
        const sql = `
        UPDATE votes
        SET status = ${obj.like}
        WHERE users_username = '${obj.username}'
        && reviews_id = ${obj.reviewId}
        `
        await pool.query(sql);

        if (obj.like === 1) {
            const sql3 = `
                UPDATE points
                SET value = value + 3
                WHERE users_username =(SELECT users_username 
                FROM reviews
                WHERE id = ${obj.reviewId} )
                `
            await pool.query(sql3);
        } else {
            const sql3 = `
                UPDATE points
                SET value = value - 3
                WHERE users_username =(SELECT users_username 
                FROM reviews
                WHERE id = ${obj.reviewId} )
                `
            await pool.query(sql3);
        }
    }

    const sql = `
    SELECT *
    FROM votes
    WHERE users_username = '${obj.username}'
    && reviews_id = ${obj.reviewId}
    `
    const vote = await pool.query(sql)
    const check3 = await pool.query(`
        SELECT users_username 
        FROM points
        WHERE 
        users_username IN (SELECT users_username 
        FROM reviews
        WHERE books_id = ${obj.booksId});
        `)
    await userService.checkPoints(check3[0]);
    return vote[0];
}

const rateBook = async (obj) => {
    const sql = `
    UPDATE ratings
    SET rating = ${obj.rating}
    WHERE users_username = '${obj.username}'
    && books_id = ${obj.id};`
    await pool.query(sql)

    const check = await pool.query(`
    SELECT * FROM ratings
    WHERE rating = ${obj.rating} 
    && users_username = '${obj.username}' 
    && books_id = ${obj.id};
    `)
    if (check.length === 0) {
        const sql = `
        INSERT INTO ratings 
        (rating, books_id, users_username)
        VALUES 
        (0, ${obj.id}, '${obj.username}')
        `
        await pool.query(sql)
    }
    const result = await pool.query(`
    SELECT
    * from ratings
    WHERE users_username = '${obj.username}'
    && books_id = ${obj.id}
    `);
    return result[0];
}

const getVoteStatus = async (obj) => {
    const status = await pool.query(`
    SELECT
    status 
    FROM 
    votes
    WHERE users_username = '${obj.username}'
    && reviews_id = ${obj.reviewId};
    `);
    return status[0];
}

const getRate = async (obj) => {
    const sql = await pool.query(`
    select rating from ratings 
    WHERE users_username = '${obj.username}'
    && bookS_id = ${obj.id};
    `);
    return sql[0];
}

const deleteVote = async (obj) => {
    const sql = `
    DELETE
    FROM
    votes
    WHERE users_username = '${obj.username}'
    && reviews_id = ${obj.reviewId}
    `
    return await pool.query(sql)
}

const createBook = async (obj) => {
    const check = await pool.query(`
    SELECT * 
    FROM books 
    WHERE title = '${obj.title}' 
    && author = '${obj.author}' 
    && is_deleted = 0`);
    if (check[0]) {
        return null;
    }
    await pool.query(`
    INSERT INTO books 
    (title, author, year, is_deleted, genre, img_src)
    VALUES 
    ('${obj.title}', '${obj.author}', ${obj.year}, false, '${obj.genre}', '${obj.img}');`)
    const createdBook = await pool.query(`
    SELECT * from books 
    WHERE title = '${obj.title}' 
    && author = '${obj.author}'
    && year = ${obj.year} 
    && genre = '${obj.genre}'
    && img_src = '${obj.img}' 
    `);
    return createdBook[0];
}

const hasBorrowedAndReturned = async (obj) => {
    const result = await pool.query(`
    SELECT *
    FROM is_borrowed
    WHERE users_username = '${obj.username}'
    && books_id = ${obj.id}
    && is_deleted = true
    `)
    return result[0]
}

const updateBook = async (id, obj) => {
    if (obj.title) {
        await pool.query(`
        UPDATE books
        SET title = '${obj.title}' 
        WHERE id = ${id}`);
    }
    if (obj.author) {
        await pool.query(`
        UPDATE books 
        SET author = '${obj.author}' 
        WHERE id = ${id}`);
    }
    if (obj.year) {
        await pool.query(`
        UPDATE books 
        SET year = ${obj.year} 
        WHERE id = ${id}`);
    }
    if (obj.genre) {
        await pool.query(`
        UPDATE books 
        SET genre = '${obj.genre}' 
        WHERE id = ${id}`);
    }
    if (obj.img) {
        await pool.query(`
        UPDATE books 
        SET img_src = '${obj.img}' 
        WHERE id = ${id}`);
    }
    const updatedBook = await bookById(id);
    return updatedBook;
}

const deleteBook = async (id) => {
    await pool.query(`
    UPDATE books 
    SET is_deleted = true 
    WHERE id = ${id};
    `)
    const deletedBook = await pool.query(`
    SELECT * 
    FROM books 
    WHERE id = ${id}`);
    return deletedBook[0];
}
export default {
    bookById,
    borrow,
    returnBook,
    getAllBooks,
    addBookReview,
    updateReview,
    reviewById,
    allReviews,
    updateReview,
    deleteReview,
    likeReview,
    getVoteStatus,
    deleteVote,
    createBook,
    rateBook,
    getRate,
    hasBorrowedAndReturned,
    updateBook,
    deleteBook,
}