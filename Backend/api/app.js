import { PORT } from './config.js';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';

import authController from './controllers/auth-controller.js';
import userController from './controllers/user-controller.js';
import bookController from './controllers/book-controller.js';
import adminController from './controllers/admin-controller.js';
//import { checkBlacklist } from './middlewares/isTokenBlacklisted.js';

const app = express();

passport.use(jwtStrategy);

app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());

app.use('/sessions', authController);
app.use('/users', userController);

//app.use(checkBlacklist);
app.use('/books', bookController);
app.use('/admin', adminController);

app.listen(PORT, () => console.log('listening'));