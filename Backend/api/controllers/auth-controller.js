import express from 'express';
import userData from '../data/user-data.js'
import userService from '../services/user-service.js'
import serviceErrors from '../services/service-errors.js'
import createToken from './../auth/createToken.js'
import { authMiddleware } from '../auth/auth-middleware.js'
import atob from 'atob'
import { checkBlacklist } from '../middlewares/isTokenBlacklisted.js';

const authController = express.Router();

authController
    .post('/', async (req, res) => {
        const { username, password } = req.body;
        const { error, user } = await userService.login(userData)(username, password);

        if (error === serviceErrors.INVALID_LOGIN) {
            res.status(400).send({ message: 'Invalid username or password' })
        } else if (error === serviceErrors.RECORD_DUPLICATE) {
            res.status(409).send({ message: 'User already logged in.' })
        } else {
            const payload = {
                sub: user.id,
                username: user.username,
                role: user.roles_id
            };

            const token = createToken(payload);
            res.status(200).send({
                token: token
            });
        }
    })
    .delete('/', authMiddleware, /*checkBlacklist,*/ async (req, res) => {
        const token = req.headers.authorization.split(' ')[1];
        const username = (JSON.parse(atob(token.split('.')[1]))).username;
        console.log(username);
        const result = await userService.logOut(userData)(token, username);
        res.status(200).send(result.res);
    })

export default authController;