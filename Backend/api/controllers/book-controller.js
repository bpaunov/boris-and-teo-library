import express from 'express';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import bookData from '../data/book-data.js';
import bookService from '../services/book-service.js';
import serviceErrors from '../services/service-errors.js';
//import { createBookSchema, createReviewSchema, updateBookSchema } from '../validations/index.js';
//import { createValidator } from '../validations/validator-middleware.js';
import { checkBlacklist } from '../middlewares/isTokenBlacklisted.js'
import atob from 'atob';

const bookController = express.Router();

bookController
    .get('/', authMiddleware, /*checkBlacklist,*/ async (req, res) => {
        const { title } = req.query;
        const { author } = req.query;
        const allBooks = await bookService.getAllBooks(bookData)()
        const filteredData = allBooks.filter(el => typeof el.id === 'number');

        if (title) {
            const newArr = filteredData.filter(el => el.title.toLowerCase().includes(title.toLowerCase()))
            if (author) {
                const finalArr = newArr.filter(el => el.author.toLowerCase().includes(author.toLowerCase()))
                res.status(200).send(finalArr)
            } else {
                res.status(200).send(newArr)
            }
        }
        else if (author) {
            const newArr = filteredData.filter(el => el.author.toLowerCase().includes(author.toLowerCase()))
            res.status(200).send(newArr)
        } else {
            res.status(200).send(filteredData)
        }
    })
    .get('/:id', authMiddleware, /*checkBlacklist,*/ async (req, res) => {
        const { id } = req.params;
        const result = await bookService.getBookById(bookData)(+id);

        if (result.error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Book not found' });
        } else if (result.book) {
            res.status(200).send(result.book);
        };
    })
    .post('/:id', authMiddleware, /*checkBlacklist,*/ async (req, res) => {
        const { id } = req.params;
        const token = req.headers.authorization.split(' ')[1];
        const username = (JSON.parse(atob(token.split('.')[1]))).username;


        const result = await bookService.borrowBook(bookData)({ id: +id, user: username });
        console.log(result);
        if (result.error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Book not found' })
        } else if (result.error === serviceErrors.RECORD_DUPLICATE) {
            res.status(409).send({ message: 'Book already borrowed' })
        } else {
            res.status(200).send(result.book)
        }
    })
    .delete('/:id', authMiddleware, /*checkBlacklist,*/ async (req, res) => {
        const { id } = req.params;
        const token = req.headers.authorization.split(' ')[1];
        const username = (JSON.parse(atob(token.split('.')[1]))).username;
        const userId = (JSON.parse(atob(token.split('.')[1]))).id;


        const result = await bookService.returnBook(bookData)({ id: +id, user: username, userId: userId });

        if (result.error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Book not found' });
        } else {
            res.status(200).send(result.book);
        };
    })
    .get('/:id/reviews', authMiddleware, /*checkBlacklist,*/ async (req, res) => {
        const { id } = req.params;
        const result = await bookService.getAllReviews(bookData)(+id);

        res.status(201).send(result.review);
    })
    .post('/:id/reviews', authMiddleware, /*checkBlacklist,*/ async (req, res) => {
        const { id } = req.params;
        const { content } = req.body;
        const token = req.headers.authorization.split(' ')[1];
        const username = (JSON.parse(atob(token.split('.')[1]))).username;

        const result = await bookService.postReview(bookData)({ bookId: id, username: username, content: content });

        if (result.error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Book not found' });
        } else {
            res.status(201).send(result.review);
        };
    })
    .get('/:id/reviews/:review_id', authMiddleware, checkBlacklist, async (req, res) => {
        const { id } = req.params;
        const { review_id } = req.params;
        const review = await bookService.getReviewById(bookData)(+review_id);

        if (review.error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Review not found' });
        } else {
            res.status(200).send(review.review)
        }
    })
    .put('/:id/reviews/:review_id', authMiddleware,/* checkBlacklist,*/ async (req, res) => {
        const { content } = req.body;
        const { review_id } = req.params;
        const token = req.headers.authorization.split(' ')[1];
        const username = JSON.parse(atob(token.split('.')[1])).username;
        const role = JSON.parse(atob(token.split('.')[1])).role;

        if (role === 1) {
            const result = await bookService.getReviewById(bookData)(+review_id);
            if (result.error === serviceErrors.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'Review not found' });
            } else {
                await bookService.updateReview(bookData)(+review_id, content);
                const result3 = await bookService.getReviewById(bookData)(+review_id);
                res.status(200).send(result3.review)
            }
        } else {
            const check = await bookService.getReviewById(bookData)(+review_id);
            if (check.error === serviceErrors.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'Review not found' });
            } else if (check.review.users_username !== username) {
                res.status(403).send({ message: 'forbidden resource' });
            } else {
                await bookService.updateReview(bookData)(+review_id, content);
                const result3 = await bookService.getReviewById(bookData)(+review_id);
                res.status(200).send(result3.review)
            }
        }
    })

    .delete('/:id/reviews/:reviewId', authMiddleware, checkBlacklist, async (req, res) => {
        const { ...p } = req.params;

        const token = req.headers.authorization.split(' ')[1];
        const username = (JSON.parse(atob(token.split('.')[1]))).username;
        const role = (JSON.parse(atob(token.split('.')[1]))).role;

        if (role === 1) {
            const result = await bookService.deleteReview(bookData)({ bookId: +p.id, username: username, reviewId: +p.reviewId });
            if (result.error === serviceErrors.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'Review not found' });
            } else if (result.error === serviceErrors.REVIEW_ALREADY_DELETED) {
                res.status(400).send({ message: 'Already deleted' });
            } else {
                res.status(200).send(result.review);
            }
        } else {
            const check = await bookService.getReviewById(bookData)(+p.reviewId);

            if (check.review.users_username !== username) {
                res.status(403).send({ message: 'Forbidden resource' });
            }
            const result = await bookService.deleteReview(bookData)({ bookId: +p.id, username: username, reviewId: +p.reviewId });

            if (result.error === serviceErrors.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'Review not found' });
            } else if (result.error === serviceErrors.REVIEW_ALREADY_DELETED) {
                res.status(400).send({ message: 'Already deleted' });
            } else {
                res.status(200).send(result.review);
            }
        }
    })
    .put('/:id/reviews/:reviewId/votes', authMiddleware,/* checkBlacklist,*/ async (req, res) => {
        const { like } = req.body;
        const { ...p } = req.params;
        const reviewUsername = await (await bookService.getReviewById(bookData)(+p.reviewId)).review;

        const result = await bookService.likeReview(bookData)({ booksId: +p.id, reviewId: +p.reviewId, username: reviewUsername.users_username, like: +like });
        if (result.error === serviceErrors.RECORD_DUPLICATE) {
            res.status(402).send({ message: 'Review already liked/disliked.' })
        }
        if (result.error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Review not found' });
        } else {
            res.status(200).send(result.vote)
        }
    })
    .get('/:id/reviews/:reviewId/votes', authMiddleware, /*checkBlacklist,*/ async (req, res) => {
        const { ...p } = req.params;
        const reviewUsername = await (await bookService.getReviewById(bookData)(+p.reviewId)).review;

        const result = await bookService.getLike(bookData)({
            reviewId: +p.reviewId,
            username: reviewUsername.users_username
        });
        if (result.error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Not found' });
        }
        res.status(200).send(result.vote)
    })
    .put('/:id/rate', authMiddleware,/* checkBlacklist,*/ async (req, res) => {
        const { rating } = req.body;
        const token = req.headers.authorization.split(' ')[1];
        const username = (JSON.parse(atob(token.split('.')[1]))).username;
        const { ...p } = req.params;

        const result = await bookService.rateBook(bookData)({ id: +p.id, username: username, rating: rating });

        if (result.error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Cannot rate this book.' });
        } else {
            res.status(200).send(result.rating)
        }
    })
    .get('/:id/rate', authMiddleware, /*checkBlacklist,*/ async (req, res) => {
        const token = req.headers.authorization.split(' ')[1];
        const username = (JSON.parse(atob(token.split('.')[1]))).username;
        const { ...p } = req.params;

        const result = await bookService.getRate(bookData)({ id: +p.id, username: username });
        res.status(200).send(result.rating)
    })



export default bookController;