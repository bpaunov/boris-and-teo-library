import express from 'express';
import userData from '../data/user-data.js';
import userService from '../services/user-service.js'
import serviceErrors from '../services/service-errors.js'
import bookData from '../data/book-data.js';
import bookService from '../services/book-service.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import { createBookSchema, createReviewSchema, updateBookSchema } from '../validations/index.js';
import { createValidator } from '../validations/validator-middleware.js';
import atob from 'atob';


const adminController = express.Router();



adminController

    .get('/books', async (req, res) => {
        const { title } = req.query;
        const { author } = req.query;
        if (title && author) {
            res.redirect(`http://localhost:3000/books?title=${title}&author=${author}`);
        } else if (title && !author) {
            res.redirect(`http://localhost:3000/books?title=${title}`);
        } else if (author && !title) {
            res.redirect(`http://localhost:3000/books?author=${author}`);
        } else {
            res.redirect(`http://localhost:3000/books`);
        }
    })
    .get('/books/:id', async (req, res) => {
        const { id } = req.params;
        res.redirect(`http://localhost:3000/books/${id}`);
    })
    .post('/books', authMiddleware, roleMiddleware(1), createValidator(createBookSchema), async (req, res) => {
        const { ...bodyData } = req.body;
        const result = await bookService.createBook(bookData)(bodyData);
        if (result.error === serviceErrors.RECORD_DUPLICATE) {
            res.status(409).send({ message: 'Book with that author and title already exists' });
        } else {
            res.status(200).send(result.book);
        }
    })
    // rosen admin eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEyLCJ1c2VybmFtZSI6IlJvc2VuIiwicm9sZSI6IkFkbWluIiwiaWF0IjoxNjAzMTMxMzQwLCJleHAiOjE2MDMxMzQ5NDB9.InwQ-71DUx_jzYtmFEznEOuuG3NI-C8CjgSZEEXCVEQ

    // pesho common eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlBlc2hvIiwicm9sZSI6IkNvbW1vbiIsImlhdCI6MTYwMzEyOTA4MiwiZXhwIjoxNjAzMTMyNjgyfQ.b0e7ppMNCO1WG_Q33cBtsxwBZct08BZNaVakFfkC8q8
    .put('/books/:id', authMiddleware, roleMiddleware(1), createValidator(updateBookSchema), async (req, res) => {
        const { id } = req.params;
        const { ...bodyData } = req.body;
        const result = await bookService.updateBook(bookData)(+id, bodyData);
        if (result.error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Book not found' });
        } else {
            res.status(200).send(result.book);
        }
    })
    .delete('/books/:id', authMiddleware, roleMiddleware(1), async (req, res) => {
        const { id } = req.params;
        const result = await bookService.deleteBook(bookData)(+id);
        if (result.error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Book not found' });
        } else {
            res.status(200).send(result.book);
        }
    })
    .post('/books/:id/reviews', authMiddleware, roleMiddleware(1), async (req, res) => {
        const { id } = req.params;
        res.redirect(307, `http://localhost:3000/books/${id}/reviews`)
    })
    .get('/books/:id/reviews/:reviewId', authMiddleware, roleMiddleware(1), async (req, res) => {
        const { id, reviewId } = req.params;
        res.redirect(307, `http://localhost:3000/books/${id}/reviews/${reviewId}`);
    })
    .put('/books/:id/reviews/:reviewId', authMiddleware, roleMiddleware(1), async (req, res) => {
        const { id, reviewId } = req.params;
        res.redirect(307, `http://localhost:3000/books/${id}/reviews/${reviewId}`);
    })
    .delete('/books/:id/reviews/:reviewId', authMiddleware, roleMiddleware(1), async (req, res) => {
        const { id, reviewId } = req.params;
        res.redirect(307, `http://localhost:3000/books/${id}/reviews/${reviewId}`);
    })
    .post('/users/:id/banStatus', authMiddleware, roleMiddleware(1), async (req, res) => {
        const { reason, duration } = req.body;
        const { id } = req.params;

        const result = await userService.banUser(userData)(+id, reason, duration);
        if (result.error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: "Not found" });
        } else if (result.error === serviceErrors.RECORD_DUPLICATE) {
            res.status(400).send({ message: " User is already banned. " });
        } else {
            res.status(200).send(result.user);
        }
    })
    .delete('/users/:id', authMiddleware, roleMiddleware(1), async (req, res) => {
        const { ...p } = req.params;

        const deletedUser = await userService.deleteUser(userData)(+p.id)
        if (deletedUser.error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: "No such user exists." })
        }
        res.status(200).send(deletedUser.user);
    });

export default adminController