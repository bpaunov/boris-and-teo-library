import express from 'express';
import userService from '../services/user-service.js';
import userData from '../data/user-data.js';
import serviceErrors from '../services/service-errors.js';
import atob from 'atob';

const userController = express.Router();

userController
    .post('/', async (req, res) => {
        const { ...b } = req.body;
        const result = await userService.createUser(userData)(b);

        if (result.error === serviceErrors.RECORD_DUPLICATE) {
            res.status(409).send({ message: 'Profile with that username already exists' });
        } else {
            res.status(200).send(result.user);
        }
    })
    .get('/:id', async (req, res) => {
        const { ...p } = req.params;
        const points = await userData.checkPoints(+p.id);

        if (!points) {
            res.status(400).send({ message: 'Something went wrong' })
        } else {
            res.status(200).send(points)
        }
    })



export default userController;
