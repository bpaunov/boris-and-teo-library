export const updateReview = {
    
    lastUpdate: {
        type: Date,
        default: Date.now
    },
    content: value => {
        if (!value) {
            return 'Review content cannot be blank'
        }
        if (typeof value != 'string') {
            return 'Review must be a string'
        }
        if (value.length <= 20 || value.length >= 1500) {
            return 'The review must be between 20 and 1500 characters long.'
        }
    },
}