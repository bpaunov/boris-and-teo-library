export const updateUserSchema = {
    name: value => {
        if (!value) {
            return null
        }
        if (typeof value != 'string'){
            return 'The name should be a string'
        }
        if (value.length < 2 || value.length > 25) {
            return 'The name should be between 3 and 24 characters.'
        }
        return null
    }
}