import { genres } from '../../config.js';

export const checkGenre = (value, object) => {
    for (const genre in object) {
        if (value === object[genre]) {
            return true;
        }
    }
    return false;
}
export const createBookSchema = {
    author: value => typeof value === 'string' && value.length >= 1 && value.length <= 30,
    title: value => typeof value === 'string' && value.length >= 1 && value.length <= 100,
    year: value => typeof value === 'number' && value >= 1 && value <= 3000,
    genre: value => typeof value === 'string' && value.length >= 1 && value.length <= 35 && checkGenre(value, genres),
    img: value => typeof value === 'string' && value.length !== 0
}