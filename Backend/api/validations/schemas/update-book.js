import { genres } from '../../config.js';
import { checkGenre } from './create-book.js'
export const updateBookSchema = {
    title: value => {
        if (!value) {
            return true;
        }
        if (typeof value !== 'string' || value.length < 1 || value.length > 100) {
            return undefined;
        } else {
            return true;
        }
    },
    author: value => {
        if (!value) {
            return true;
        }
        if (typeof value !== 'string' || value.length < 1 || value.length > 15) {
            return undefined;
        } else {
            return true;
        }
    },
    genre: value => {
        if (!value) {
            return true;
        }
        if (typeof value !== 'string' || value.length < 1 || value.length > 35 || !(checkGenre(value, genres))) {
            return undefined;
        } else {
            return true;
        }
    },
    year: value => {
        if (!value) {
            return true;
        }
        if (typeof value !== 'number' || value < 1 || value > 3000) {
            return undefined;
        } else {
            return true;
        }
    },
    img: value => {
        if (!value) {
            return true;
        }
        if (typeof value !== 'string' || value.length === 0) {
            return undefined;
        } else {
            return true;
        }
    }
}