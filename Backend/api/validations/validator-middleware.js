export const createValidator = schema => {

    return (req, res, next) => {
        const body = req.body;
        const validations = Object.keys(schema);

        const fails = validations
            .map(el => schema[el](body[el]))
            .filter(el => el === undefined)
        if (fails.length > 0) {
            res.status(400).send(fails)
        } else {

            next();
        }
    }
}