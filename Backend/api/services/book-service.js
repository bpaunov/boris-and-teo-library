import serviceErrors from './service-errors.js';
import userService from './user-service.js';

const getAllBooks = bookData => {
    return async () => {
        return await bookData.getAllBooks();
    }
}

const getBookById = bookData => {
    return async (id) => {
        const book = await bookData.bookById(id);
        if (!book) {
            return { error: serviceErrors.RECORD_NOT_FOUND, book: null };
        }
        return { error: null, book: book };
    }
}

const borrowBook = bookData => {
    return async (obj) => {
        const check = await getBookById(bookData)(obj.id);
        if (!check) {
            return { error: serviceErrors.RECORD_NOT_FOUND, book: null }
        } else {
            const check2 = await bookData.borrow(obj);
            if (!check2) {
                return { error: serviceErrors.RECORD_DUPLICATE, book: null }
            } else {
                return { error: null, book: check2 }
            }
        }
    }
}

const returnBook = bookData => {
    return async (obj) => {
        const checkIfExists = await getBookById(bookData)(obj.id);
        if (checkIfExists.book === null) {
            return { error: serviceErrors.RECORD_NOT_FOUND, book: null };
        }
        const result = await bookData.returnBook(obj);
        if (!result) {
            return { error: serviceErrors.RECORD_NOT_FOUND, book: null };
        }
        await userService.checkPoints(obj.userId);
        return { error: null, book: result };
    }
}
const getReviewById = bookData => {
    return async (reviewId) => {
        const review = await bookData.reviewById(reviewId);
        if (!review) {
            return { error: serviceErrors.RECORD_NOT_FOUND, review: null };
        }
        return { error: null, review: review };
    }
}

const getAllReviews = bookData => {
    return async (bookId) => {
        const reviews = await bookData.allReviews(bookId);
        if (!reviews[0]) {
            return { error: serviceErrors.RECORD_NOT_FOUND, review: null }
        }
        return { error: null, review: reviews };
    }
}

const postReview = bookData => {
    return async (reviewObject) => {
        if (!await getBookById(bookData)(reviewObject.bookId)) {
            return { error: serviceErrors.RECORD_NOT_FOUND, review: null };
        } else {
            const result = await bookData.addBookReview(reviewObject);
            return { error: null, review: result };
        }
    }
}

const updateReview = bookData => {
    return async (reviewId, reviewNewText) => {
        const review = await getReviewById(bookData)(reviewId);
        if (!review) {
            return { error: serviceErrors.RECORD_NOT_FOUND, review: null }
        } else {
            await bookData.updateReview(+reviewId, reviewNewText);
            const updatedReview = await getReviewById(bookData)(+reviewId);
            return { error: null, review: updatedReview }
        }
    }
}

const deleteReview = bookData => {
    return async (obj) => {
        const check = await getBookById(bookData)(obj.bookId);
        if (check.book === null) {
            return { error: serviceErrors.RECORD_NOT_FOUND, review: null };
        }
        const result = await bookData.deleteReview(obj);
        if (result === undefined) {
            return { error: serviceErrors.RECORD_NOT_FOUND, review: null };
        } else if (result === null) {
            return { error: serviceErrors.REVIEW_ALREADY_DELETED, review: null };
        } else {
            return { error: null, review: result };
        }
    }
}

const likeReview = bookData => {
    return async (obj) => {
        const check = await getReviewById(bookData)(obj.reviewId);
        if (check.error === serviceErrors.RECORD_NOT_FOUND) {
            return check.error;
        }
        const result = await bookData.likeReview(obj)
        if (result === null) {
            return { error: serviceErrors.RECORD_DUPLICATE, vote: null }
        }
        if (result === undefined) {
            return { error: serviceErrors.RECORD_NOT_FOUND, vote: null };
        } else {
            return { error: null, vote: result };
        }
    }
}

const getLike = bookData => {
    return async (obj) => {
        const check = await getReviewById(bookData)(obj.reviewId);
        if (check.error === serviceErrors.RECORD_NOT_FOUND) {
            return check.error;
        }
        const result = await bookData.getVoteStatus(obj);
        return { error: null, vote: result };
    }
}

const rateBook = bookData => {
    return async (obj) => {
        await getBookById(bookData)(obj.id);
        const check = await bookData.hasBorrowedAndReturned(obj);
        if (check === undefined) {
            return { error: serviceErrors.RECORD_NOT_FOUND, borrowed: null }
        }
        const result = await bookData.rateBook(obj)
        if (result === undefined) {
            return { error: serviceErrors.RECORD_NOT_FOUND, rating: null };
        } else {
            return { error: null, rating: result };
        }

    }
}

const getRate = bookData => {
    return async (obj) => {
        const result = await bookData.getRate(obj);
        return { error: null, rating: result };
    }
}

const createBook = bookData => {
    return async (obj) => {
        const createdBook = await bookData.createBook(obj);
        if (createdBook === null) {
            return { error: serviceErrors.RECORD_DUPLICATE, book: null };
        }
        return { error: null, book: createdBook };
    }
}

const updateBook = bookData => {
    return async (id, obj) => {
        const check = await getBookById(bookData)(id);
        if (!check) {
            return { error: serviceErrors.RECORD_NOT_FOUND, book: null };
        }
        const updatedBook = await bookData.updateBook(id, obj);
        return { error: null, book: updatedBook };
    }
}

const deleteBook = bookData => {
    return async (id) => {
        const check = await getBookById(bookData)(id);
        if (!check) {
            return { error: serviceErrors.RECORD_NOT_FOUND, book: null };
        }
        const deletedBook = await bookData.deleteBook(id);
        return { error: null, book: deletedBook };
    }
}
export default {
    getBookById,
    borrowBook,
    returnBook,
    getAllBooks,
    getReviewById,
    postReview,
    updateReview,
    getAllReviews,
    deleteReview,
    likeReview,
    getLike,
    createBook,
    rateBook,
    getRate,
    updateBook,
    deleteBook
}