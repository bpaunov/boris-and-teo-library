import serviceErrors from './service-errors.js';
import bcrypt from 'bcrypt';
import { DEFAULT_USER_ROLE, pool } from '../config.js';

const createUser = userData => {
    return async (userInfo) => {
        const cryptPass = await bcrypt.hash(userInfo.password, 10); /*userInfo.password;*/
        const check = await userData.getUser(userInfo.username);

        if (check !== undefined) {
            return { error: serviceErrors.RECORD_DUPLICATE, user: null };
        } else {
            const res = await userData.createUser({ username: userInfo.username, password: cryptPass, role: DEFAULT_USER_ROLE });
            const userId = await userData.getUser(userInfo.username)
            await userData.createRecords(userInfo.username, userId.id);
            return { error: null, user: res };
        }
    }
}

const logOut = userData => {
    return async (token, username) => {
        const res = await userData.logOut(token, username);
        return { error: null, res: res };
    }
}

const banUser = userData => {
    return async (id, reason, duration) => {
        const user = await userData.getUserById(id);
        if (!user) {
            return { error: serviceErrors.RECORD_NOT_FOUND, user: null };
        }
        const result = await userData.banUser(id, reason, duration);
        if (result === null) {
            return { error: serviceErrors.RECORD_DUPLICATE, user: null }
        }
        await userData.banPointsDeduction(id, duration);
        return { error: null, user: result };

    }
}
const deleteUser = userData => {
    return async (id) => {
        const user = await userData.deleteUser(id)

        if (user === undefined) {
            return { error: serviceErrors.RECORD_NOT_FOUND, user: user }
        } else {
            return { error: null, user: user }
        }
    }
}
const checkPoints = userData => {
    return async (id) => {
        const points = await userData.checkPoints(+id);

        switch (true) {
            case points <= 50:
                break;
            case points > 50 && points <= 100:
                await userData.updateLevel(id, 1);
                break;
            case points > 100 && points <= 200:
                await userData.updateLevel(id, 2);
                break;
            case points > 200 && points <= 350:
                await userData.updateLevel(id, 3);
                break;
            case points > 350 && points <= 550:
                await userData.updateLevel(id, 4);
                break;
            case points > 550:
                await userData.updateLevel(id, 5);
                break;
        }
    }
}

const login = userData => {
    return async (username, password) => {
        const user = await userData.getUser(username);
        if (!user || !(await bcrypt.compare(password, user.password))) {
            return {
                error: serviceErrors.INVALID_LOGIN,
                user: null
            }
        }

        const session = await userData.addNewSession(username);
        if (!session) {
            return { error: serviceErrors.RECORD_DUPLICATE, session: null }
        }
        await checkPoints(userData)(user.id);
        return { error: null, user: user }
    }
}

export default {
    createUser,
    login,
    logOut,
    banUser,
    deleteUser,
    checkPoints,
}
