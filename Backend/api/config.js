import mariadb from 'mariadb';

export const pool = mariadb.createPool({
    host: 'localhost',
    database: 'library',
    user: 'root',
    password: '0000',
    port: '3306'
});

export const poolBlacklist = mariadb.createPool({
    host: 'localhost',
    database: 'blacklist_tokens_library',
    user: 'root',
    password: '0000',
    port: '3306'
})

export const PORT = 3000;

export const PRIVATE_KEY = 'mega_taen_ne64upen_klu4';

export const TOKEN_LIFETIME = 60 * 60;
export const DEFAULT_USER_ROLE = 'Common';

export const genres = {
    comedy: 'COMEDY',
    adventure: 'ADVENTURE',
    horror: 'HORROR',
    sciFi: 'SCIENCE_FICTION',
    drama: 'DRAMA',
    fantasy: 'FANTASY'
}