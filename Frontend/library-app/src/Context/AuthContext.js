import React from 'react';
import { createContext } from 'react';

export const AuthContext = createContext({
    user: null,
    setUser: () => { }
})

export const useAuth = () => React.useContext(AuthContext);

export default AuthContext;