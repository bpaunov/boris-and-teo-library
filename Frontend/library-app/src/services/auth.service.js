import axios from "axios";
import Constants from "../constants.js";

const BASE_API_URL = 'http://localhost:3000';
const API_URL = 'http://localhost:3000/sessions';
const API_URL2 = 'http://localhost:3000/users';
const API_URL_BOOKS = 'http://localhost:3000/books';

const AuthService = {
    login(username, password) {
        return axios.post(API_URL + "/", {
            username,
            password,
        })
            .then(res => {
                if (res.data.message) {
                    alert(res.data.message);
                }
                else if (res.data.token) {
                    localStorage.setItem("user", (res.data.token))
                }
                return res.data.token;
            });
    },

    Logout() {
        const headers = {
            Authorization: 'Bearer ' + localStorage.getItem("user")
        }
        return axios.delete(API_URL + "/", { headers });
    },

    register(username, password) {
        return axios.post(`${API_URL2}`, {
            username,
            password
        })
    },

    getCurrentUser() {
        return JSON.parse(localStorage.getItem("user"));
    },

    getBooks(title, author) {
        return axios.get(`${API_URL_BOOKS}?title=${title}&author=${author}`, {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem("user")
            },
        })
    },

    getSingleBook(id) {
        return axios.get(`${API_URL_BOOKS}/${id}`, {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem("user")
            },
        })
    },

    getReviews(id) {
        return axios.get(`${API_URL_BOOKS}/${id}/reviews`, {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem("user")
            },
        })
    },

    postReview(id, username, content) {
        return axios.post(`${API_URL_BOOKS}/${id}/reviews`, { content: content, username: username }, {
            headers: {
                Authorization: `Bearer ${Constants.token}`
            }
        })
    },

    borrowBook(id) {
        return axios.post(`${API_URL_BOOKS}/${id}`, {}, {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem("user")
            },
        })
    },

    returnBook(id) {
        return axios.delete(`${API_URL_BOOKS}/${id}`, {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem("user")
            }
        })
    },

    updateReview(id, content, review_id) {
        const headers = {
            Authorization: `Bearer ${Constants.token}`
        }
        const data = {
            id: id,
            content: content,
            review_id: review_id
        }
        return axios.put(`${API_URL_BOOKS}/${id}/reviews/${review_id}`, data, { headers })
    },

    deleteReview(id, review_id) {
        const headers = {
            Authorization: 'Bearer ' + localStorage.getItem("user")
        }
        return axios.delete(`${API_URL_BOOKS}/${id}/reviews/${review_id}`, { headers })
    },

    likeReview(bookId, reviewId, like) {
        const headers = {
            Authorization: `Bearer ${Constants.token}`
        }
        return axios.put(`${API_URL_BOOKS}/${bookId}/reviews/${reviewId}/votes`, { like }, { headers: headers })
    },

    getLikeOfReview(bookId, reviewId) {
        const headers = {
            Authorization: `Bearer ${Constants.token}`
        }
        return axios.get(`${API_URL_BOOKS}/${bookId}/reviews/${reviewId}/votes`, { headers: headers })
    },

    getRateOfBook(bookId) {
        const headers = {
            Authorization: `Bearer ${Constants.token}`
        }
        return axios.get(`${API_URL_BOOKS}/${bookId}/rate`, { headers: headers })
    },

    rateBook(bookId, rating) {
        const headers = {
            Authorization: `Bearer ${Constants.token}`
        }
        return axios.put(`${API_URL_BOOKS}/${bookId}/rate`, { rating }, { headers: headers })
    },

    banUser(id, reason, duration) {
        const headers = {
            Authorization: `Bearer ${Constants.token}`
        }
        return axios.post(`${BASE_API_URL}/admin/users/${id}/banStatus`, { reason, duration }, { headers: headers })
    },

    deleteUser(id) {
        const headers = {
            Authorization: `Bearer ${Constants.token}`
        }
        return axios.delete(`${BASE_API_URL}/admin/users/${id}`, { headers: headers })
    },

    createBook(title, author, year, genre, img) {
        const headers = {
            Authorization: `Bearer ${Constants.token}`
        };
        return axios.post(`${BASE_API_URL}/admin/books`, { title, author, year, genre, img }, { headers: headers })
    },

    updateBook(id, title, author, year, genre, img) {
        const headers = {
            Authorization: `Bearer ${Constants.token}`
        };
        return axios.put(`${BASE_API_URL}/admin/books/${id}`, { title, author, year, genre, img }, { headers: headers })
    },
    getPoints(id) {
        const headers = {
            Authorization: 'Bearer ' + localStorage.getItem("user")
        }
        return axios.get(`${API_URL2}/${id}`, { headers })
    },
    deleteBook(id) {
        const headers = {
            Authorization: `Bearer ${Constants.token}`
        };
        return axios.delete(`${BASE_API_URL}/admin/books/${id}`, { headers: headers })
    }
}


export default AuthService;