import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:3000';

const UserService = {

    getUserBoard(){
        return axios.get(API_URL + '/', { headers: authHeader() })
    },
    getAdminBoard(){
        return axios.get(API_URL + '/Admin', { headers: authHeader() })
    }
}

export default UserService;