import React, { useState } from 'react';
import AuthService from "../../services/auth.service.js"
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';

const Return = ({ id }) => {
  const [message, setMesage] = useState("");

  if (message) {
    return (
      <div>
        <h2>{message}</h2>
      </div>
    )
  }

  const returnFn = (bookId) => {
    AuthService.returnBook(bookId)
      .then(data => {
        if (data.message) {
          throw new Error(`${data.message}`);
        } else {
          setMesage("Book returned.");
        }
      })
      .catch(err => setMesage(`${err}`))
  }
  return (
    <div className="return button">
      <Button variant="outline-primary" onClick={() => returnFn(id)}>Return this book.</Button>
    </div>
  )
}

export default Return;