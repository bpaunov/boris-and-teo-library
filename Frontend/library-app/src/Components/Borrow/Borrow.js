import React, { useState } from 'react';
import AuthService from "../../services/auth.service.js"
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';

const Borrow = ({ id }) => {
  const [message, setMesage] = useState("");

  if (message) {
    return (
      <div>
        <h2>{message}</h2>
      </div>
    )
  }

  const borrowFn = (bookId) => {
    console.log(bookId);
    AuthService.borrowBook(bookId)
      .then(data => {
        console.log(data)
        if (data.message) {
          throw new Error(`${data.message}`);
        } else {
          setMesage("The book has been borrowed.");
        }
      })
      .catch(err => setMesage(`${err}`))
  }
  return (
    <div>
      <Button variant="outline-primary" onClick={() => borrowFn(id)}>Borrow this book.</Button>
    </div>
  )
}

export default Borrow;