import React, { useState } from 'react';
import authService from '../../services/auth.service.js';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';
import './signUp.css';

const SignUp = () => {
    const [registration, setSignUpState] = useState(false);

    const [signUpInfo, setSignUpInfo] = useState({ username: null, password: null });

    const signUpFn = ({ username, password }) => {
        if (!username || !password) {
            alert('There is missing info');
            return;
        }
        setSignUpState(null);
        authService.register(username, password)
            .then(data => {
                if (data.message) {
                    throw new Error(`${data.message}`);
                } else {
                    setSignUpInfo({ username: null, password: null });
                }
            })
            .catch((err) => setSignUpState(false))
            .finally(() => setSignUpState(true));
    }

    if (registration === false) {
        return (
            <Form className='RegisterForm'>
            <h3>Sign Up</h3>
            
            <div className="form-group">
                <label>Username</label>
                <input type="text" className="form-control" placeholder="Username" onChange={(e) => signUpInfo.username = e.target.value} />
            </div>
            
            <div className="form-group">
                <label>Password</label>
                <input type="password" className="form-control" placeholder="Enter password" onChange={(e) => signUpInfo.password = e.target.value} />
            </div>
            
            <button type="submit" className="btn btn-primary btn-block" onClick={() =>  signUpFn({ ...signUpInfo }) }>Sign Up</button>
            <p className="forgot-password text-right">
                Already registered <a href="#">sign in?</a>
            </p>
            </Form>
        // <div>
        //     <input type='text' placeholder='Username' onChange={(e) => signUpInfo.username = e.target.value}></input>
        //     <input type='text' placeholder='Password' onChange={(e) => signUpInfo.password = e.target.value}></input>
        //     <Button variant="outline-primary" onClick={() =>  signUpFn({ ...signUpInfo }) }>Sign Up</Button>
        // </div>
        )
    }
    if (registration) {
        return (
            <div>
                <h1>You are registered</h1>
            </div>
        )
    }
    if (registration === null) {
        return (
            <div>
                <h1>Loading...</h1>
            </div>
        )
    }
    return (
        <div>
            <h1>Something is wrong...</h1>
        </div>
    )
}


export default SignUp;