import React, { useContext } from 'react';
import AuthService from "../../services/auth.service.js"
import { AuthContext } from '../../Context/AuthContext';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';

const Logout = () => {
  const { user, setUser } = useContext(AuthContext);

  const LogoutFn = () => {

    AuthService.Logout()
      .then(data => {
        if (data.message) {
          alert(`${data.message}`);
        } else {
          localStorage.removeItem('user');
          setUser(null);
          alert('Logout Successful.')
        }
      })
  }

  return (

    <Button variant="outline-primary" onClick={LogoutFn}>Logout</Button>

  )
}

export default Logout;