import React, { useState } from 'react';
import AuthService from '../../../services/auth.service';
import Err from '../../Error/Error.js';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';

const BanUsers = ({ banFormSet }) => {
    const [message, setMessage] = useState(false);
    const [isBanFormValid, setIsBanFormValid] = useState(false);
    const [banForm, setBanForm] = useState({
        targetedUserId: {
            placeholder: 'Id of the targeted user...',
            value: '',
            validations: {
                required: true,
                minValue: 0
            },
            valid: false,
            touched: false
        },
        reason: {
            placeholder: 'Reason for ban...',
            value: '',
            validations: {
                required: true,
                minLength: 5
            },
            valid: false,
            touched: false
        },
        duration: {
            placeholder: 'Duration of ban',
            value: '',
            validations: {
                required: true,
                minValue: 1,
                maxValue: 7
            },
            valid: false,
            touched: false
        }
    });

    const isInputValid = (validation, value) => {
        let isValid = true;
        if (validation.required) {
            isValid = isValid && value.length !== 0;
        }
        if (validation.minLength) {
            isValid = isValid && value.length >= validation.minLength
        }
        if (validation.minValue) {
            isValid = isValid && value >= validation.minValue
        }
        if (validation.maxValue) {
            isValid = isValid && value <= validation.maxValue
        }
        return isValid;
    }

    const handleInputChange = event => {
        const { name, value } = event.target;
        const updatedControl = { ...banForm[name] };

        updatedControl.value = value;
        updatedControl.touched = true;
        updatedControl.valid = isInputValid(updatedControl.validations, value);

        const updatedForm = { ...banForm, [name]: updatedControl };
        setBanForm(updatedForm);

        const formValid = Object.values(updatedForm).every(control => control.valid);
        setIsBanFormValid(formValid);
    }

    const formElements = Object.keys(banForm)
        .map(name => ({ name, config: banForm[name] }))
        .map(({ name, config }) => {
            return (
                <input type='text' key={name} name={name} placeholder={config.placeholder} value={config.value} onChange={handleInputChange} />
            )
        });

    const ban = (ev) => {
        ev.preventDefault();
        AuthService.banUser(
            banForm.targetedUserId.value,
            banForm.reason.value,
            banForm.duration.value)
            .then(data => {
                if (data.message) {
                    throw new Error(`${data.data.message}`)
                } else {
                    setMessage('User banned');
                }
            })
            .catch(err => setMessage(`${err.message}`))
            .finally(() => setTimeout(() => banFormSet(), 1000));
    }

    if (message) {
        return (
            <div>
                <Err message={message} />
            </div>
        )
    }

    return (
        <div>
            <Form onSubmit={ban}>
                {formElements}
                <Button variant="outline-primary" type='submit' disabled={!isBanFormValid}>Ban user</Button>
            </Form>
        </div>
    )
}

export default BanUsers;