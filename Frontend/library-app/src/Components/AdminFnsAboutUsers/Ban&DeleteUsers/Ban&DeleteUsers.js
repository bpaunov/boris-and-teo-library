import React, { useState } from 'react';
import Constants from '../../../constants.js';
import atob from 'atob';
import BanUsers from './BanUsers.js';
import DeleteUsers from './DeleteUsers.js';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';

const BanAndDeleteUsers = () => {
    const [banForm, setBanForm] = useState(false);
    const [deleteForm, setDeleteForm] = useState(false);

    if (JSON.parse(atob(Constants.token.split('.')[1])).role !== 1) {
        return (<div>
            <h1>Error 401: Forbidden resource</h1>
        </div>);
    }

    const backToBanAnDelete = () => {
        setBanForm(false);
        setDeleteForm(false)
    }

    return (
        <div>
            {banForm ? <BanUsers banFormSet={backToBanAnDelete} /> : banForm === null ? null : <Button variant="outline-primary" onClick={() => {
                setBanForm(true);
                setDeleteForm(null);
            }}>Open Ban Field</Button>}
            {deleteForm ? <DeleteUsers deleteFormSet={backToBanAnDelete} /> : deleteForm === null ? null : <Button variant="outline-primary" onClick={() => {
                setDeleteForm(true)
                setBanForm(null);
            }}>Open Delete Field</Button>}
            {(deleteForm || banForm) && <Button variant="outline-primary" onClick={() => backToBanAnDelete()}>Close field</Button>}
        </div>
    )
}

export default BanAndDeleteUsers;