import React, { useState } from 'react';
import AuthService from '../../../services/auth.service';
import Err from '../../Error/Error.js';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';

const DeleteUsers = ({ deleteFormSet }) => {
    const [message, setMessage] = useState(false);
    const [isDeleteFormValid, setIsDeleteFormValid] = useState(false);
    const [deleteForm, setDeleteForm] = useState({
        targetedUserId: {
            placeholder: 'Id of the targeted user...',
            value: '',
            validations: {
                required: true,
                minValue: 0
            },
            valid: false,
            touched: false
        }
    });

    const isInputValid = (validation, value) => {
        let isValid = true;
        if (validation.required) {
            isValid = isValid && value.length !== 0;
        }
        if (validation.minValue) {
            isValid = isValid && value >= validation.minValue
        }
        return isValid;
    }

    const handleInputChange = event => {
        const { name, value } = event.target;
        const updatedControl = { ...deleteForm[name] };

        updatedControl.value = value;
        updatedControl.touched = true;
        updatedControl.valid = isInputValid(updatedControl.validations, value);

        const updatedForm = { ...deleteForm, [name]: updatedControl };
        setDeleteForm(updatedForm);

        const formValid = Object.values(updatedForm).every(control => control.valid);
        setIsDeleteFormValid(formValid);
    }

    const formElements = Object.keys(deleteForm)
        .map(name => ({ name, config: deleteForm[name] }))
        .map(({ name, config }) => {
            return (
                <input type='text' key={name} name={name} placeholder={config.placeholder} value={config.value} onChange={handleInputChange} />
            )
        });

    const del = (ev) => {
        ev.preventDefault();
        AuthService.deleteUser(deleteForm.targetedUserId.value)
            .then(data => {
                if (data.message) {
                    throw new Error(`${data.data.message}`)
                } else {
                    setMessage('User deleted');
                }
            })
            .catch(err => setMessage(`${err.message}`))
            .finally(() => setTimeout(() => deleteFormSet(), 1000));
    }

    if (message) {
        return (
            <div>
                <Err message={message} />
            </div>
        )
    }

    return (
        <div>
            <Form onSubmit={del}>
                {formElements}
                <Button variant="outline-primary" type='submit' disabled={!isDeleteFormValid}>Ban user</Button>
            </Form>
        </div>
    )
}

export default DeleteUsers;