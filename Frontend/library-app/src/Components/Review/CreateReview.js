import React, { useState } from 'react';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';

const CreateReview = ({ create }) => {
    const [isFormValid, setIsFormValid] = useState(false);
    const [form, setForm] = useState({
        username: {
            placeholder: 'Username',
            touched: false,
            validations: {
                required: true,
                minLength: 3,
                maxLength: 50,
            },
            valid: false,
            value: '',
        },
        content: {
            placeholder: 'Comment',
            touched: false,
            validations: {
                required: true,
                minLength: 3,
                maxLength: 50,
            },
            valid: false,
            value: '',
        }
    });

    const isInputValid = (validation, value) => {
        let isValid = true;
        if (validation.required) {
            isValid = isValid && value.length !== 0;
        }
        if (validation.minLength) {
            isValid = isValid && value.length >= validation.minLength
        }
        if (validation.maxLength) {
            isValid = isValid && value.length <= validation.maxLength
        }
        return isValid;
    }

    const handleInputChange = event => {
        const { name, value } = event.target;
        const updatedControl = { ...form[name] };

        updatedControl.value = value;
        updatedControl.touched = true;
        updatedControl.valid = isInputValid(updatedControl.validations, value);

        const updatedForm = { ...form, [name]: updatedControl };
        setForm(updatedForm);

        const formValid = Object.values(updatedForm).every(control => control.valid);
        setIsFormValid(formValid);
    }

    const createEvent = ev => {
        ev.preventDefault();
        create(form);
    }

    const formElements = Object.keys(form)
        .map(name => ({ name, config: form[name] }))
        .map(({ name, config }) => {
            return (
                <input type='text' key={name} name={name} placeholder={config.placeholder} value={config.value} onChange={handleInputChange} />
            )
        });

    return (
        <div>
            <Form onSubmit={createEvent}>
                {formElements}
                <Button variant="outline-primary" type='submit' disabled={!isFormValid}>Create</Button>
            </Form>
        </div>
    )

}

export default CreateReview;