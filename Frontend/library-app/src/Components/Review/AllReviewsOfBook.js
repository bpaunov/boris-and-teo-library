import React from 'react';
import ReviewView from './reviewView.js';

const Review = ({ reviews }) => {
    return (
        <div>
            {reviews && reviews.map(review => <ReviewView key={review.id} review={review} />)}
        </div>
    )
}

export default Review