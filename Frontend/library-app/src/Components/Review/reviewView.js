import React, { useEffect, useState } from 'react';
import AuthService from '../../services/auth.service';
import UpdateReview from '../Review/UpdateReview';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';

const ReviewView = ({ review }) => {

    const [updateReviewField, setUpdateReviewField] = useState(false);

    const [retrieve, setRetrieveState] = useState(false);
    const [error, setError] = useState(null);
    const [author, setAuthor] = useState();
    const [like, setLike] = useState(false);

    useEffect(() => {
        AuthService.getLikeOfReview(review.books_id, review.id)
            .then((data) => {
                data.data.status === 1 ? setLike(true) : setLike(false);
            })
    }, [review]);

    const updateReview = (content) => {
        AuthService.updateReview(review.books_id, content, review.id)
            .then(data => {
                if (data.message) {
                    throw new Error(`${data.message}`);
                } else {
                    setUpdateReviewField(false);
                }
            })
            .catch(err => setError(`${err}`))
            .finally(() => setRetrieveState(false));
    }

    const deleteReview = (e) => {
        e.preventDefault();
        AuthService.deleteReview(review.books_id, review.id)
            .then(data => {
                if (data.message) {
                    throw new Error(`${data.message}`);
                } else {
                    setUpdateReviewField(false);
                }
            })
    }

    const Vote = ev => {
        ev.preventDefault();
        AuthService.likeReview(review.books_id, review.id, !like ? 1 : 0)
            .then((data) => {
                if (data.message) {
                    throw new Error(`${data.message}`)
                } else {
                    setLike(!like);
                }
            })
            .catch((err) => setError(`${err}`));
    }

    return (
        <div>
            <h3>By: {review.users_username}</h3>
            <p>{review.content}</p>
            <p>Published on - {review.created_at}</p>
            {like ?
                <Form onSubmit={Vote}>
                    <Button variant="outline-primary" type='submit'>Liked</Button>
                </Form> :
                <Form onSubmit={Vote}>
                    <Button variant="outline-primary" type='submit'>Like</Button>
                </Form>}

            {!updateReviewField &&
                (JSON.parse(atob(localStorage.getItem('user').split('.')[1])).username === review.users_username ||
                    JSON.parse(atob(localStorage.getItem('user').split('.')[1])).role === 1)
                && <Button variant="outline-primary" onClick={() => setUpdateReviewField(true)}>Edit</Button>}
            {updateReviewField && <UpdateReview review={review} updateFn={updateReview} />}
            {updateReviewField && <Form onSubmit={deleteReview}> <input type='submit' value='Delete review.' /> </Form>}
            {updateReviewField && <Button variant="outline-primary" onClick={() => setUpdateReviewField(false)}>Cancel</Button>}
        </div>
    )
}

export default ReviewView