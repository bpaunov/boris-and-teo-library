import React, { useState } from 'react';
import Loading from '../Loader/Loader';
import Err from '../Error/Error'
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';

const UpdateReview = ({ review, updateFn }) => {
    console.log(review);

    const [content, setContent] = useState(review.content);

    const [retrieve,] = useState(false);
    const [error, setError] = useState(null);

    if (retrieve) {
        return (
            <div>
                <Loading />
            </div>
        )
    }

    if (error) {
        return (
            <div>
                <Err message={error} />
            </div>
        )
    }
    const handleSubmit = (e) => {
        updateFn(content);
    }
    const handleChange = (e) => {
        setContent(e.target.value);
    }
    return (
        <Form onSubmit={handleSubmit}>
            <label>
                Review content:
                <input type='text' value={content} onChange={handleChange} />
                <input type='submit' value='Update' />
            </label>
        </Form>
    )
}

export default UpdateReview;