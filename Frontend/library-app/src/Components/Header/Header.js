import { NavLink } from 'react-router-dom';
import { AuthContext } from '../../Context/AuthContext';
import React, { useContext, useState } from 'react';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';
import Logout from '../Logout/Logout';
import Points from '../Header/Points';

const Header = () => {
    const { user, setUser } = useContext(AuthContext);

    return (
        <div>
            <Navbar bg="light" expand="lg">
                <Navbar.Brand href="#home">Nqkvo epi4no ime</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link href="http://localhost:4000/home">Home</Nav.Link>
                        <Nav.Link href="http://localhost:4000/Books">Books</Nav.Link>
                        <Nav.Link href="http://localhost:4000/SignUp">Register</Nav.Link>
                        
                        <>
                        Hello, {user?.username ?? 'nobody'}
                        {user ? <Logout /> : <NavLink to="/Login"><Button variant="outline-primary" variant="outline-primary">Login</Button></NavLink>}
                        </>
                        {user ? <Points /> : null}
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </div>
    )
};

/*
<h2>Online Library</h2>

            <nav className="navigation">

                <NavLink to="/SignUp"><button>SignUp</button></NavLink>
                <NavLink to="/Books"><button>Books</button></NavLink>
                
                
            </nav>
*/
export default Header;