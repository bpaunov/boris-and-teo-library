import React, { useEffect, useState } from 'react';
import AuthService from '../../services/auth.service';

const Points = () => {
    const [points, setPoints] = useState(null);
    const id_id = JSON.parse(atob(localStorage.getItem('user').split('.')[1])).sub;
    useEffect(() => {
        AuthService.getPoints(id_id)
            .then(data => {
                if (data.message) {
                    throw new Error(`${data.message}`)
                } else {
                    setPoints(Object.values(data)[0].value);
                }
            })
    }, [])

    return (
        <div>You currently have points {points}</div>
    )
}

export default Points;
