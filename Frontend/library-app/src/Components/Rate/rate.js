import React, { Fragment, useEffect, useState } from 'react';
import AuthService from '../../services/auth.service.js';
import Err from '../Error/Error';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';

const Rate = ({ id }) => {
    const [error, setError] = useState(null);
    const [rating, setRating] = useState({ rating: null });
    const [isFormValid, setIsRateFormValid] = useState(false);
    const [rateForm, setRateForm] = useState({
        rating: {
            placeholder: 'Your rating...',
            touched: false,
            validations: {
                required: true,
                minValue: 0,
                maxValue: 5,
            },
            valid: false,
            value: '',
        }
    });

    useEffect(() => {
        AuthService.getRateOfBook(id)
            .then(data => {
                if (data.message) {
                    throw new Error(`${data.message}`);
                } else {
                    setRateForm({
                        rating: {
                            placeholder: typeof data.data.rating === 'number' ? `Your rate: ${data.data.rating}` : `Your rating...`,
                            touched: false,
                            validations: {
                                required: true,
                                minValue: 0,
                                maxValue: 5,
                            },
                            valid: false,
                            value: '',
                        }
                    })
                }
            })
            .catch(err => setError(`${err}`))
    }, [rating]);

    const isInputValid = (validation, value) => {
        let isValid = true;
        if (validation.required) {
            isValid = isValid && value.length !== 0;
        }
        if (validation.minValue) {
            isValid = isValid && +value >= validation.minValue;
        }
        if (validation.maxValue) {
            isValid = isValid && +value <= validation.maxValue;
        }
        return isValid;
    }

    const handleInputChange = event => {
        const { name, value } = event.target;
        const updatedControl = { ...rateForm[name] };

        updatedControl.value = value;
        updatedControl.touched = true;
        updatedControl.valid = isInputValid(updatedControl.validations, value);

        const updatedForm = { ...rateForm, [name]: updatedControl };
        setRateForm(updatedForm);
        const formValid = Object.values(updatedForm).every(control => control.valid);
        setIsRateFormValid(formValid);
    }

    const rate = (ev) => {
        ev.preventDefault();
        AuthService.rateBook(id, rateForm.rating.value)
            .then((data) => {
                if (data.message) {
                    throw new Error(`${data.message}`)
                } else {
                    setRating({ rating: rateForm.rating.value });
                }
            })
            .catch((err) => setError(`${err}`));
    }

    if (error) {
        return (
            <div>
                <Err message={error} />
            </div>
        )
    }

    return (
        <Fragment>
            <Form onSubmit={rate}>
                <input type='text' name={'rating'} placeholder={rateForm.rating.placeholder} value={rateForm.rating.value} onChange={handleInputChange} />
                <Button variant="outline-primary" type='submit' disabled={!isFormValid}>Rate</Button>
            </Form>
        </Fragment>
    )
}

export default Rate;