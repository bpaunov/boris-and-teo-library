import React, { useState } from 'react';
import authService from '../../services/auth.service.js';
import BookView from './bookView.js';
import { NavLink } from 'react-router-dom';
import Loading from '../Loader/Loader.js';
import Err from '../Error/Error';
import atob from 'atob';
import Constants from '../../constants.js';
import CreateBook from '../AdminFnsAboutBooks/CreateBook.js';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl, Container, Col, Row } from 'react-bootstrap';
import './BookContainer.css';

const Books = () => {
    const [retrieve, setRetrieveState] = useState(false);

    const [error, setError] = useState(null);

    const [searchInfo, setSearchInfo] = useState({ title: '', author: '' });

    const [books, setBooks] = useState(false);

    const [createBook, setCreateBook] = useState(false);

    const [showCreateBookField, setShowCreateBookField] = useState(false);

    let countBooksOnRow = 0;

    if (JSON.parse(atob(Constants.token.split('.')[1])).role === 1) {
        if (!createBook) {
            setCreateBook(true);
        }
    }

    const searchFn = ({ title, author }) => {
        setRetrieveState(true);
        authService.getBooks(title, author)
            .then(data => {
                if (data.message) {
                    throw new Error(`${data.message}`)
                } else {
                    setBooks([...data.data]);
                    setSearchInfo({ title: '', author: '' });
                }
            })
            .catch(err => setError(`${err}`))
            .finally(() => setRetrieveState(false));
    }

    if (retrieve) {
        return (
            <div>
                <Loading />
            </div>
        )
    }

    if (error) {
        return (
            <div>
                <Err message={error} />
            </div>
        )
    }

    const closeShowCreateField = () => {
        setShowCreateBookField(false);
    }

    const clearFn = () => {
        setBooks(false);
    }

    return (
        <div>
            <p>Search by title/author:</p>
            <Form inline>
                <FormControl type='text' placeholder='Title' onChange={(e) => searchInfo.title = e.target.value} className="mr-sm-2" />
                <FormControl type='text' placeholder='Author' onChange={(e) => searchInfo.author = e.target.value} className="mr-sm-2" />
                <Button variant="outline-primary" onClick={() => searchFn({ ...searchInfo })}>Search</Button>
            </Form>
            <br />
            {createBook && <Button variant="outline-primary" onClick={() => setShowCreateBookField(true)}>Create Book</Button>}
            {showCreateBookField && <CreateBook closeShowCreateField={closeShowCreateField} />}
            {books && books.map(book => {
                countBooksOnRow++;
                if (countBooksOnRow === 5 || countBooksOnRow % 5 === 0) {
                    return (
                        <Row>
                            <Col sm key={books[countBooksOnRow - 5].id}>
                                <BookView book={books[countBooksOnRow - 5]} />
                                <br />
                                <NavLink to={`/Books/${books[countBooksOnRow - 5].id}`}><button>View book</button></NavLink>
                            </Col>
                            <Col sm key={books[countBooksOnRow - 4].id}>
                                <BookView book={books[countBooksOnRow - 4]} />
                                <br />
                                <NavLink to={`/Books/${books[countBooksOnRow - 4].id}`}><button>View book</button></NavLink>
                            </Col>
                            <Col sm key={books[countBooksOnRow - 3].id}>
                                <BookView book={books[countBooksOnRow - 3]} />
                                <br />
                                <NavLink to={`/Books/${books[countBooksOnRow - 3].id}`}><button>View book</button></NavLink>
                            </Col>
                            <Col sm key={books[countBooksOnRow - 2].id}>
                                <BookView book={books[countBooksOnRow - 2]} />
                                <br />
                                <NavLink to={`/Books/${books[countBooksOnRow - 2].id}`}><button>View book</button></NavLink>
                            </Col>
                            <Col sm key={books[countBooksOnRow - 1].id}>
                                <BookView book={books[countBooksOnRow - 1]} />
                                <br />
                                <NavLink to={`/Books/${books[countBooksOnRow - 1].id}`}><button>View book</button></NavLink>
                            </Col>
                        </Row>
                    )
                }
                if (countBooksOnRow === books.length) {
                    let indexes = [];
                    const findNearestDeleteOnFive = (givenValue, count) => {
                        indexes.push(givenValue);
                        if (givenValue % 5 === 0) {
                            return count;
                        }
                        return findNearestDeleteOnFive(givenValue - 1, count + 1)
                    }
                    findNearestDeleteOnFive(countBooksOnRow - 1, 0);
                    return (
                        <Row>
                            {indexes.map(i => {
                                return (
                                    <Col sm key={books[i].id}>
                                        <BookView book={books[i]} />
                                        <br />
                                        <NavLink to={`/Books/${books[i].id}`}><button>View book</button></NavLink>
                                    </Col>
                                )
                            })}
                        </Row>
                    )

                }
                return (
                    <div key={book.id}>
                        <BookView book={book} />
                        <br />
                        <NavLink to={`/Books/${book.id}`}><Button variant="outline-primary">View book</Button></NavLink>
                    </div>
                )
            })}
            {books && <Button variant="outline-primary" onClick={() => clearFn()}>Clear</Button>}
        </div>
    )
}

export default Books;