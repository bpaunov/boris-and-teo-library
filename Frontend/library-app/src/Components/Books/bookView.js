/* eslint-disable jsx-a11y/alt-text */
import React from 'react';

const BookView = ({ book }) => {
    return (
        <div className='BookView'>
            <img src={book.img_src} />
            <p>{book.title} - {book.author} - {book.year} - {book.genre}</p>
        </div>
    )
}

export default BookView