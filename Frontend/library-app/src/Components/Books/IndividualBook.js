import React, { useEffect, useState } from 'react';
import Loading from '../Loader/Loader.js';
import AuthService from '../../services/auth.service';
import Err from '../Error/Error';
import Reviews from '../Review/AllReviewsOfBook.js';
import CreateReview from '../Review/CreateReview.js';
import BookView from './bookView.js';
import { NavLink } from 'react-router-dom';
import Rate from '../Rate/rate.js';
import Constants from '../../constants.js';
import UpdateBook from '../AdminFnsAboutBooks/UpdateBook.js';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';
import Return from '../Return/Return.js';
import Borrow from '../Borrow/Borrow.js';

const ViewSingleBook = (props) => {
    const [retrieve, setRetrieveState] = useState(false);
    const [error, setError] = useState(null);
    const [book, setSingleBook] = useState(null);
    const [bookReviews, setBookReviews] = useState(null);
    const [createReviewField, setCreateReviewField] = useState(false);
    const [updateBook, setUpdateBook] = useState(false);
    const [showUpdateBookField, setShowUpdateBookField] = useState(false);
    const [deleteBookButton, setDeleteBookButton] = useState(false);

    const bookId = props.match.params.id;

    if (JSON.parse(atob(Constants.token.split('.')[1])).role === 1) {
        if (!updateBook && !deleteBookButton) {
            setUpdateBook(true);
            setDeleteBookButton(true);
        }
    }

    useEffect(() => {
        setRetrieveState(true);
        AuthService.getSingleBook(bookId)
            .then(data => {
                if (data.message) {
                    throw new Error(`${data.message}`);
                } else {
                    setSingleBook({ ...data.data });
                }
            })
            .catch(err => setError(`${err}`))
            .finally(() => setRetrieveState(false));
    }, [bookId])

    useEffect(() => {
        AuthService.getReviews(bookId)
            .then(data => {
                if (data.message) {
                    throw new Error(`${data.message}`);
                } else {
                    setBookReviews([...data.data]);
                }
            })
            .catch(err => setError(`${err}`))
    }, [book]);

    const create = ({ username, content }) => {
        AuthService.postReview(bookId, username.value, content.value)
            .then(data => {

                if (data.message) {
                    throw new Error(`${data.message}`);
                } else {
                    setSingleBook({ ...book });
                }
            })
            .catch(err => setError(`${err}`))
            .finally(() => setCreateReviewField(false));
    }

    const deleteBook = (ev) => {
        ev.preventDefault();
        AuthService.deleteBook(bookId)
            .then(data => {

                if (data.message) {
                    throw new Error(`${data.message}`);
                } else {
                    setRetrieveState(false);
                    setError('Book deleted');
                }
            })
            .catch(err => setError(`${err}`))
            .finally(() => setTimeout(() => window.location.assign('http://localhost:4000/Books'), 2000));
    }

    if (retrieve) {
        return (
            <div>
                <Loading />
            </div>
        )
    }

    if (error) {
        return (
            <div>
                <Err message={error} />
            </div>
        )
    }

    const closeShowUpdateField = () => {
        setShowUpdateBookField(false);
    }

    return (
        <div>
            <NavLink to='/Books'><Button variant="outline-primary">Go back</Button></NavLink>
            
            {book && <BookView book={book} />}
            <Borrow id={bookId} /><Return id={bookId} />
            
            {deleteBookButton && <Button variant="outline-primary" onClick={deleteBook}>Delete Book</Button>}
            {updateBook && <Button variant="outline-primary" onClick={() => setShowUpdateBookField(true)}>Update Book</Button>}
            {showUpdateBookField && <UpdateBook book={book} closeShowUpdateField={closeShowUpdateField} />}

            {book && <Rate id={bookId} />}
            {!createReviewField && <Button variant="outline-primary" onClick={(ev) => {
                ev.preventDefault();
                setCreateReviewField(true);
            }}>Add review</Button>}

            {createReviewField && <CreateReview create={create} />}

            {createReviewField && <Button variant="outline-primary" onClick={() => setCreateReviewField(false)}>Cancel</Button>}

            {bookReviews && <Reviews reviews={bookReviews} />}
            
        </div>
    );
}

export default ViewSingleBook;