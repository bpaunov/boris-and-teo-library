import React, { useEffect, useState } from 'react';
import AuthService from '../../services/auth.service';
import Err from '../Error/Error.js';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';

const UpdateBook = ({ book, closeShowUpdateField }) => {
    const [message, setMessage] = useState(false);
    const [isUpdateFormValid, setIsUpateFormValid] = useState(false);
    const [updateForm, setUpdateForm] = useState({
        title: {
            placeholder: `${book.title}`,
            touched: false,
            validations: {
                required: true,
                minLength: 4,
                maxLength: 20,
            },
            value: book.title,
            valid: false
        },
        author: {
            placeholder: `${book.author}`,
            touched: false,
            validations: {
                required: true,
                minLength: 4,
                maxLength: 20,
            },
            value: book.author,
            valid: false
        },
        year: {
            placeholder: `${book.year}`,
            touched: false,
            validations: {
                required: true,
                minLength: 4,
                minValue: 1900,
                maxValue: 3000,
            },
            value: book.year,
            valid: false
        },
        genre: {
            placeholder: `${book.genre}`,
            touched: false,
            validations: {
                required: true,
                minLength: 4,
                maxLength: 20,
            },
            value: book.genre,
            valid: false
        },
        img: {
            placeholder: `${book.img_src}`,
            touched: false,
            validations: {
                required: true,
                minLength: 1
            },
            value: book.img_src,
            valid: false
        }
    });

    const isInputValid = (validation, value) => {
        let isValid = true;
        if (validation.required) {
            isValid = isValid && value.length !== 0;
        }
        if (validation.minLength) {
            isValid = isValid && value.length >= validation.minLength
        }
        if (validation.maxLength) {
            isValid = isValid && value.length <= validation.maxLength
        }
        if (validation.minValue) {
            isValid = isValid && +value >= validation.minValue;
        }
        if (validation.maxValue) {
            isValid = isValid && +value <= validation.maxValue;
        }

        return isValid;
    }

    const handleInputChange = event => {
        const { name, value } = event.target;
        const updatedControl = { ...updateForm[name] };

        updatedControl.value = value;
        updatedControl.touched = true;
        updatedControl.valid = isInputValid(updatedControl.validations, value);

        const updatedForm = { ...updateForm, [name]: updatedControl };
        setUpdateForm(updatedForm);

        const formValid = Object.values(updatedForm).every(control => control.value);
        setIsUpateFormValid(formValid);
    }

    const updateEvent = ev => {
        ev.preventDefault();
        AuthService.updateBook(
            book.id,
            updateForm.title.value,
            updateForm.author.value,
            updateForm.year.value,
            updateForm.genre.value,
            updateForm.img.value
        )
            .then(data => {
                if (data.message) {
                    throw new Error(`${data.message}`)
                } else {
                    setMessage('Book updated!')
                }
            })
            .catch((err) => setMessage(`${err.message}`))
            .finally(() => setTimeout(() => {
                closeShowUpdateField()
                window.location.assign(`http://localhost:4000/Books/${book.id}`)
            }, 2000));
    }

    const formElements = Object.keys(updateForm)
        .map(name => ({ name, config: updateForm[name] }))
        .map(({ name, config }) => {
            return (
                <input type='text' key={name} name={name} placeholder={config.placeholder} value={config.value} onChange={handleInputChange} />
            )
        });

    if (message) {
        return (
            <div>
                <Err message={message} />
            </div>
        )
    }

    return (
        <div>
            <Form onSubmit={updateEvent}>
                {formElements}
                <Button variant="outline-primary" type='submit' disabled={!isUpdateFormValid}>Update</Button>
            </Form>
        </div>
    )
}

export default UpdateBook