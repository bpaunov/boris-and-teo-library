import React, { useState } from 'react';
import AuthService from '../../services/auth.service';
import Err from '../Error/Error.js';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';

const CreateBook = ({ closeShowCreateField }) => {
    const [message, setMessage] = useState(false);
    const [isCreateFormValid, setIsCreateFormValid] = useState(false);
    const [createForm, setCreateForm] = useState({
        title: {
            placeholder: 'Title...',
            touched: false,
            validations: {
                required: true,
                minLength: 4,
                maxLength: 20,
            },
            value: '',
            valid: false
        },
        author: {
            placeholder: 'Author...',
            touched: false,
            validations: {
                required: true,
                minLength: 4,
                maxLength: 20,
            },
            value: '',
            valid: false
        },
        year: {
            placeholder: 'Year...',
            touched: false,
            validations: {
                required: true,
                minLength: 4,
                minValue: 1900,
                maxValue: 3000,
            },
            value: '',
            valid: false
        },
        genre: {
            placeholder: 'Genre...',
            touched: false,
            validations: {
                required: true,
                minLength: 4,
                maxLength: 20,
            },
            value: '',
            valid: false
        },
        img: {
            placeholder: 'Book cover image...',
            touched: false,
            validations: {
                required: true,
                minLength: 1
            },
            value: '',
            valid: false
        }
    });

    const isInputValid = (validation, value) => {
        let isValid = true;
        if (validation.required) {
            isValid = isValid && value.length !== 0;
        }
        if (validation.minLength) {
            isValid = isValid && value.length >= validation.minLength
        }
        if (validation.maxLength) {
            isValid = isValid && value.length <= validation.maxLength
        }
        if (validation.minValue) {
            isValid = isValid && +value >= validation.minValue;
        }
        if (validation.maxValue) {
            isValid = isValid && +value <= validation.maxValue;
        }

        return isValid;
    }

    const handleInputChange = event => {
        const { name, value } = event.target;
        const updatedControl = { ...createForm[name] };

        updatedControl.value = value;
        updatedControl.touched = true;
        updatedControl.valid = isInputValid(updatedControl.validations, value);

        const updatedForm = { ...createForm, [name]: updatedControl };
        setCreateForm(updatedForm);

        const formValid = Object.values(updatedForm).every(control => control.valid);
        setIsCreateFormValid(formValid);
    }

    const createEvent = ev => {
        ev.preventDefault();
        AuthService.createBook(
            createForm.title.value,
            createForm.author.value,
            createForm.year.value,
            createForm.genre.value,
            createForm.img.value
        )
            .then(data => {
                if (data.message) {
                    throw new Error(`${data.message}`)
                } else {
                    setMessage('Book created!')
                }
            })
            .catch((err) => setMessage(`${err}`))
            .finally(() => setTimeout(() => closeShowCreateField(), 2000));
    }

    const formElements = Object.keys(createForm)
        .map(name => ({ name, config: createForm[name] }))
        .map(({ name, config }) => {
            return (
                <input type='text' key={name} name={name} placeholder={config.placeholder} value={config.value} onChange={handleInputChange} />
            )
        });

    if (message) {
        return (
            <div>
                <Err message={message} />
            </div>
        )
    }

    return (
        <div>
            <Form onSubmit={createEvent}>
                {formElements}
                <Button variant="outline-primary" type='submit' disabled={!isCreateFormValid}>Create</Button>
            </Form>
        </div>
    )
}

export default CreateBook