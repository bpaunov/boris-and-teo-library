import React, { useState, useContext } from 'react';
import AuthService from "../../services/auth.service.js"
import "./Login.css";
import decode from 'jwt-decode';
import { AuthContext } from "../../Context/AuthContext";
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';

const Login = () => {
  const { setUser } = useContext(AuthContext);


  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const LoginFn = () => {
    if (!username || !password) {
      alert('There is missing information');
      return;
    }
    AuthService.login(username, password)
      .then(data => {
        if (data.message) {
          alert(`${data.message}`);
        } else {
          const user = decode(localStorage.getItem('user'));
          setUser(user);
          alert('Login Successful.')
          window.location.assign('http://localhost:4000/home');
        }
      })
  }

  return (
    <Form>
      <input
        placeholder="Username..."
        value={username}
        onChange={e => setUsername(e.target.value)}
      />
      <br />
      <input
        placeholder="Password..."
        type="password"
        value={password}
        onChange={e => setPassword(e.target.value)}
      />
      <Button variant="outline-primary" type="button" onClick={LoginFn}>Login</Button>
    </Form>
  )
}
export default Login;