
const Constants = {
    token: localStorage.getItem("user") || null,
}

export default Constants;