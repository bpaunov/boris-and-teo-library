import React, { useState } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import './App.css';
import Books from './Components/Books/getBooks';
import SignUp from './Components/SignUp/signUp.js';
import { AuthContext } from './Context/AuthContext';
import Header from './Components/Header/Header';
import NotFound from './Components/NotFound/NotFound';
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from './Components/Login/Login';
import Logout from './Components/Logout//Logout';
import ViewSingleBook from './Components/Books/IndividualBook.js';
import BanAndDeleteUsers from './Components/AdminFnsAboutUsers/Ban&DeleteUsers/Ban&DeleteUsers.js';
import decode from 'jwt-decode';

function App() {
  const token = localStorage.getItem('user');
  const [user, setUser] = useState(token
    ? decode(token)
    : null);
  return (

    <BrowserRouter>
      <AuthContext.Provider
        value={{ user: user, setUser: setUser }}>
        <Header />

        <hr />

        <Switch>
          <Redirect path="/" exact to="/home" />
          <Route path="/Login" component={Login} />
          <Route path="/SignUp" component={SignUp} />
          <Route path="/Books" exact component={Books} />
          <Route path="/Books/:id" component={ViewSingleBook} />
          <Route path="/Logout" component={Logout} />
          <Route path="/Admin" component={BanAndDeleteUsers} />
          <Route path="*" component={NotFound} />
        </Switch>
      </AuthContext.Provider>
    </BrowserRouter>
  );
}

export default App;
